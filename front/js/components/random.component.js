function Random(selector) {
    Component.call(this, selector);
    this.numbers = [];
    this.countedNumbers = [];
    this.orderedArray = [];
}

Random.prototype = Object.create(Component.prototype);
Random.constructor = Random;

Random.prototype.init = function (callback) {
    const self = this;

    axios.get('http://localhost:3000/random-numbers')
        .then(function (response) {
            self.numbers = response.data.data.map(function (number) {
                return {
                    id: number
                }
            });
            self.expr();
            self.render();

            if (typeof callback !== "function") {
                callback = false;
            }
            if (callback) {
                callback()
            }
        })
        .catch(function (error) {
            console.error(error);
        });
};

Random.prototype.render = function () {
    const container = this.getDOMElement();

    this.numbers.forEach(function (number) {
        const listElement = document.createElement('li');
        listElement.classList.add('list-group-item');
        listElement.innerHTML = number.id;

        container.appendChild(listElement);
    });
};

Random.prototype.wash = function () {
    const container = this.getDOMElement();

    while (container.firstChild) {
        container.removeChild(container.firstChild);
    }
};

// Random.expr = function call(init) {
Random.prototype.expr = function () {
    const recentNumbers = [];
    const sortNumbers = [];
    const orderedArray = [];

    //collecting last 5 generated random numbers
    this.numbers.forEach((number) =>{
        recentNumbers.push(number.id);
    });

    //adding to count array recent [num]bers by how many times they occurred
    for (let i = 0, max = recentNumbers.length; i < max; i += 1) {
        num = recentNumbers[i];
        if (this.countedNumbers[num]) {
            this.countedNumbers[num] += 1;
        } else {
            this.countedNumbers[num] = 1;
        }
    }

    //changing one dimensional array into two dimensional array
    for (let orderNr in this.countedNumbers) {
        sortNumbers.push([orderNr, this.countedNumbers[orderNr]]);
    }

    //sorting numbers by times occurred
    sortNumbers.sort(function (a, b) {
        return b[1] > a[1];
    });

    //single list of how many times some numbers occured without duplication
    const timesOccurred = (function uniq(array = sortNumbers) {
        const seenNumber = {};

        return array.map(function (keyValue) {
            return keyValue[1];
        }).filter(function (item) {
            return seenNumber.hasOwnProperty(item) ? false : (seenNumber[item] = true);
        });
    })();

    ////get sorted array with grouped '1-10' numbers by how many times they were shown
    //take all sorted and focus on their index
    sortNumbers.forEach(function (valueJ, j) {
        //for every number of how many '1-10' numbers were displayed
        timesOccurred.map(function (valueK, k) {
            //find which numbers were displayed 'valueK' times
            if (valueK === sortNumbers[j][1]) {
                if (orderedArray[k] === undefined) {
                    orderedArray[k] = ([sortNumbers[j]]);
                } else {
                    orderedArray[k].push(sortNumbers[j]);
                }
            }
        })
    });
    this.orderedArray = orderedArray;
    return orderedArray;
};