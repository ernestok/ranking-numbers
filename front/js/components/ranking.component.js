function Ranking(selector) {
    Component.call(this, selector);
    this.numbers = [];
    this.allNumbers = [];
    this.orderedArray = [];
}

Ranking.prototype = Object.create(Component.prototype);
Ranking.constructor = Ranking;

Ranking.prototype.init = function () {
    const self = this;

    axios.get('http://localhost:3000/numbers')
        .then(function (response) {
            self.numbers = response.data.data.map(function (number) {
                return {
                    id: number
                }
            });

            self.render();
        })
        .catch(function (error) {
            console.error(error);
        });
};

Ranking.prototype.render = function () {
    const container = this.getDOMElement();

    this.numbers.forEach(function (number) {
        const listElement = document.createElement('li');
        listElement.classList.add('list-group-item');
        listElement.innerHTML = number.id;

        container.appendChild(listElement);
    });
};

Ranking.prototype.renderRank = function (sort) {
    const container = this.getDOMElement();
    const liCell = Array.prototype.slice.call(container.children);

    sort.forEach(function (value, i) {
        const showedTimes = sort[i][0][1];
        const whichNumber = sort[i].map(function (value) {
            return value[0];
        });
        const rankNumber = document.createElement('span');
        rankNumber.classList.add('js-rendered-numbers');
        rankNumber.style.marginLeft = '1rem';
        let time = "times";

        if (sort[i][0][1] === 1) {
            time = "time";
        }

        rankNumber.innerHTML = `${whichNumber}  occured ${showedTimes} ${time}`;
        liCell[i].appendChild(rankNumber);
    })
};

Ranking.prototype.eraze = function () {
    const container = this.getDOMElement();

    const liCell = Array.prototype.slice.call(container.children);
    for (let i = 0, max = liCell.length; i < max; i += 1) {
        while (liCell[i].childNodes.length > 1) {
            liCell[i].removeChild(liCell[i].lastChild);
        }
    }
};
