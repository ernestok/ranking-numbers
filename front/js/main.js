const ranking = new Ranking('#numbers-ranking');
const random = new Random('#numbers-latest');

window.onload = function () {
    ranking.init();
    random.init(function () {
        ranking.renderRank(random.orderedArray);
    });
};

setInterval(function () {
    random.wash();
    ranking.eraze();
    random.init(function () {
        ranking.renderRank(random.orderedArray);
    });
}, 10000);